import { Routes, RouterModule }  from '@angular/router';

import { classStats } from './classStats.component';

// noinspection TypeScriptValidateTypes
export const routes: Routes = [
  {
    path: '',
    component: classStats
  }
];

export const routing = RouterModule.forChild(routes);
