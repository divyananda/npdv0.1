import {Component, OnInit} from '@angular/core';
import { NvD3Module } from 'ng2-nvd3';

@Component({
  selector: 'classStats',
  templateUrl: './classStats.html'
})
export class classStats implements OnInit{
  constructor() {}

  options;
  data;
  chartType;

  ngOnInit() {
    
        this.options = {
          chart: {
            type: 'sunburstChart',
            height: 450,
            //  color: d3.scale.category20c(),
            duration: 250
          }
        };
    
        this.data = this.sinAndCos();
      }
    
    
      sinAndCos() {
    
        return [{
          'name': 'flare',
          'children': [
            {
              'name': 'Online',
              'children': [
    
                {
                  'name': 'Enhance',
                  'children': [
                    { 'name': 'Resolved & Classified', 'size': 2983 },
                    { 'name': 'Not Resolved but Classified', 'size': 1047 },
                    { 'name': 'Not Resolved NOr Classified', 'size': 4375 }
                  ]
                },
    
                {
                  'name': 'Rules Pattern Matching',
                  'children': [
                    { 'name': 'Resolved & Classified', 'size': 5983 },
                    { 'name': 'Not Resolved but Classified', 'size': 6047 },
                    { 'name': 'Not Resolved NOr Classified', 'size': 3375 }
                  ]
                },
    
                {
                  'name': 'Machine Learning',
                  'children': [
                    { 'name': 'Resolved & Classified', 'size': 783 },
                    { 'name': 'Not Resolved but Classified', 'size': 2147 },
                    { 'name': 'Not Resolved NOr Classified', 'size': 5475 }
                  ]
                }
              ]
            },
            {
              'name': 'Total Channel',
              'children': [
                {
                  'name': 'Enhance',
                  'children': [
                    { 'name': 'Resolved & Classified', 'size': 1983 },
                    { 'name': 'Not Resolved but Classified', 'size': 2047 },
                    { 'name': 'Not Resolved NOr Classified', 'size': 1375 }
                  ]
                },
    
                {
                  'name': 'Rules Pattern Matching',
                  'children': [
                    { 'name': 'Resolved & Classified', 'size': 1983 },
                    { 'name': 'Not Resolved but Classified', 'size': 2047 },
                    { 'name': 'Not Resolved NOr Classified', 'size': 1375 }
                  ]
                },
    
                {
                  'name': 'Machine Learning',
                  'children': [
                    { 'name': 'Resolved & Classified', 'size': 1983 },
                    { 'name': 'Not Resolved but Classified', 'size': 2047 },
                    { 'name': 'Not Resolved NOr Classified', 'size': 1375 }
                  ]
                }
              ]
            }
          ]
        }];
    
      }

}
