import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';

import { classStats } from './classStats.component';
import { routing } from './classStats.routing';

import { LineChart } from './lineChart';
import { LineChartService } from './lineChart/lineChart.service';

import { NvD3Module } from 'ng2-nvd3';

// d3 and nvd3 should be included somewhere
import 'd3';
import 'nvd3';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    routing,
    NvD3Module
  ],
  declarations: [
    classStats,
    LineChart
  ],
  providers:[
    LineChartService
  ]
})
export class ClassStatsModule { }
