import {Component} from '@angular/core';

import {LineChartService} from './lineChart.service';
import * as Chart from 'chart.js';

@Component({
  selector: 'line-chart',
  templateUrl: './lineChart.html',
  styleUrls: ['./lineChart.scss']
})

// TODO: move chart.js to it's own component
export class LineChart {

  public stackData: {};

  constructor(private lineChartService: LineChartService) {
    this.stackData = lineChartService.getData();
  }

  ngAfterViewInit() {
    this._loadStackCharts();
  }

  private _loadStackCharts() {
    let el = jQuery('.stackchart-area').get(0) as HTMLCanvasElement;
    const config = {
      type: 'line',
      data: this.stackData,
      stacked: false,
      options: {
        title: {
          display: true,
          text: 'Classification Stage analysis fir CIDs',
        },
        tooltips: {
          mode: 'index',
          intersect: false,
        },
        responsive: true,
        legend: {
          position: 'bottom',
        },
        scales: {
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Value'
            },
          }],
        },
      },
    };

    new Chart(el.getContext('2d'), config);



  }
}
