import {Injectable} from '@angular/core';
import {BaThemeConfigProvider, colorHelper} from '../../../theme';

@Injectable()
export class LineChartService {

  constructor(private _baConfig: BaThemeConfigProvider) {
  }

  randomScalingFactor() {
    return (Math.random() > 0.5 ? 1.0 : -1.0) * Math.round(Math.random() * 100);
  }

  getData() {
    let dashboardColors = this._baConfig.get().colors.dashboard;
    return {
      labels: ['20170501123456', '20170501123466', '20170501123476', '20170501123486', '20170501123496', '20170501123106', '20170501123116'],
      datasets: [{
        label: 'Enhance',
        borderColor: dashboardColors.white,
        backgroundColor: dashboardColors.white,
        fill: false,
        data: [
          123,
          456,
          233,
          312,
          321,
          321,
          312
        ],
      }, {
          label: 'Rules Pattern Matching',
          borderColor: dashboardColors.surfieGreen,
          backgroundColor: dashboardColors.surfieGreen,
          fill: false,
          data: [
            321,
            31231,
            3121,
            3121,
            3123,
            3121,
            312
          ],

        },
        {
          label: 'Machine Learning',
          borderColor: dashboardColors.gossip,
          backgroundColor: dashboardColors.gossip,
          fill: false,
          data: [
            1321,
            1231,
            4121,
            6621,
            1223,
            12121,
            1312
          ],

        }]
    };
  }
}
