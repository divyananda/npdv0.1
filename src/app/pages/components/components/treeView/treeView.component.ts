import {Component} from '@angular/core';
import {TreeModel} from 'ng2-tree';
import { GanttModule, IGanttOptions, Zooming, Task, Project } from 'ng2-gantt';

@Component({
  selector: 'tree-view',
  templateUrl: './treeView.html',
})

export class TreeView {

  tree: TreeModel = {
    value: 'Programming languages by programming paradigm',
    children: [
      {
        value: 'Object-oriented programming',
        children: [
          { value: 'Java' },
          { value: 'C++' },
          { value: 'C#' },
        ]
      },
      {
        value: 'Prototype-based programming',
        children: [
          { value: 'JavaScript' },
          { value: 'CoffeeScript' },
          { value: 'Lua' },
        ]
      }
    ]
  };

  constructor() {
  }

  options: IGanttOptions = {
    scale: {
      start: new Date(2017, 0, 1),
      end: new Date(2017, 1, 1)
    },
    zooming: Zooming[Zooming.days]
  };

  project: Project = {
    'id': 'dd10f0b6-b8a4-4b2d-a7df-b2c3d63b4a01',
    'name': 'Batch Scheduling',
    'startDate': new Date("2017-02-27T08:32:09.6972999Z"),
    'tasks': [
      {
        'id': 'ea2a8d86-1d4b-4807-844d-d5417fcf618d',
        'treePath': '20170201123456',
        'parentId': 'ea2a8d86-1d4b-4807-844d-d5417fcf618d',
        'name': '20170201123456',
        'resource': 'res1',
        'start': new Date('2017-01-01T00:00:00.0Z'),
        'end': new Date('2017-01-03T00:00:00.0Z'),
        'percentComplete': 100,
        'status': 'Completed'
      },
      {
        'id': 'dd755f20-360a-451f-b200-b83b89a35ad1',
        'treePath': '20170201123466',
        'parentId': 'dd755f20-360a-451f-b200-b83b89a35ad1',
        'name': '20170201123466',
        'resource': 'res2',
        'start': new Date('2017-01-05T00:00:00.0Z'),
        'end': new Date('2017-01-06T00:00:00.0Z'),
        'percentComplete': 0
      },
      {
        'id': 'j1b997ef-bb89-4ca2-b134-62a08a19aef6',
        'treePath': '20170201123476',
        'parentId': 'j1b997ef-bb89-4ca2-b134-62a08a19aef6',
        'name': '20170201123476',
        'resource': 'res2',
        'start': new Date('2017-01-06T00:00:00.0Z'),
        'end': new Date('2017-01-07T00:00:00.0Z'),
        'percentComplete': 0
      },
      {
        'id': 'ub12f674-d5cb-408f-a941-ec76af2ec47e',
        'treePath': '20170201123476',
        'parentId': 'ub12f674-d5cb-408f-a941-ec76af2ec47e',
        'name': '20170201123476',
        'resource': 'res1',
        'start': new Date('2017-01-07T00:00:00.0Z'),
        'end': new Date('2017-01-22T00:00:00.0Z'),
        'percentComplete': 0,
        'status': 'Error'
      },
      {
        'id': 'xafa430b-d4da-4d7d-90ed-69056a042d7a',
        'treePath': '20170201123476',
        'parentId': 'xafa430b-d4da-4d7d-90ed-69056a042d7a',
        'name': '20170201123476',
        'resource': 'res1',
        'start': new Date('2017-01-22T00:00:00.0Z'),
        'end': new Date('2017-01-23T00:00:00.0Z')
      },

      {
        'id': 'b5c071a5-430c-4d61-acf4-799cbdf61c49',
        'treePath': '20170201123476',
        'parentId': 'b5c071a5-430c-4d61-acf4-799cbdf61c49',
        'name': '20170201123476',
        'resource': 'res2',
        'start': new Date('2017-01-24T00:00:00.0Z'),
        'end': new Date('2017-01-24T00:34:00.0Z')
      },
      {
        'id': 't9ba762e-bde7-47bf-b628-75f99fdd5bef',
        'treePath': '20170201123476',
        'parentId': 't9ba762e-bde7-47bf-b628-75f99fdd5bef',
        'name': '20170201123476',
        'resource': 'res2',
        'start': new Date('2017-01-24T00:00:00.0Z'),
        'end': new Date('2017-01-24T00:00:36.0Z')
      }
    ]
  };


  groupData(array: any[], f: any): any[] {
    var groups = {};
    array.forEach((o: any) => {
      var group = JSON.stringify(f(o));

      groups[group] = groups[group] || [];
      groups[group].push(o);
    });
    return Object.keys(groups).map((group: any) => {
      return groups[group];
    });
  }

  createTask(element: any) {
    var selectedStatus = element.options[element.selectedIndex].value;

    var parentTask = {
      'id': 'parent_task_' + Math.random(),
      'parentId': 'parent_task',
      'treePath': 'parent_task',
      'name': 'parent_task',
      'percentComplete': 0,
      'start': new Date('2017-01-01T03:30:00.0Z'),
      'end': new Date('2017-01-01T12:45:00.0Z'),
      'status': selectedStatus
    }
    this.project.tasks.push(parentTask);

    var childTask = {
      'id': 'child_task_' + Math.random(),
      'parentId': 'ea2a8d86-1d4b-4807-844d-d5417fcf618d',
      'treePath': 'parent 1/child3',
      'name': 'child3',
      'percentComplete': 0,
      'start': new Date('2017-01-01T03:30:00.0Z'),
      'end': new Date('2017-01-01T12:45:00.0Z'),
      'status': selectedStatus
    }
    this.project.tasks.push(childTask);

  }

  updateTasks() {
    for (var i = 0; i < this.project.tasks.length; i++) {
      let task = this.project.tasks[i];

      let progress = setInterval(function() {
        if (task.percentComplete === 100) {
          task.status = "Completed";
          clearInterval(progress);
        } else {
          if (task.percentComplete === 25) {
            task.status = "Warning";
          } else if (task.percentComplete === 50) {
            task.status = "Error";
          } else if (task.percentComplete === 75) {
            task.status = "Information";
          }

          task.percentComplete += 1;
        }
      }, 200);
    }
  }

  loadBigDataSet() {
    var tasks = [];

    for (var i = 11; i < 1000; i++) {
      var task = {
        id: `parent${i}`,
        name: 'task testing',
        percentComplete: 0,
        start: new Date(),
        end: new Date(),
        status: ''
      }

      tasks.push(task);
    }

    this.project.tasks.push(...tasks);
  }

  gridRowClicked(event) {
    console.log(event);
  }

}
