import {Injectable} from '@angular/core';
import {BaThemeConfigProvider, colorHelper} from '../../../theme';

@Injectable()
export class StackChartService {

  constructor(private _baConfig: BaThemeConfigProvider) {
  }

  randomScalingFactor() {
    return (Math.random() > 0.5 ? 1.0 : -1.0) * Math.round(Math.random() * 100);
  }

  getData() {
    let dashboardColors = this._baConfig.get().colors.dashboard;
    return {
      labels: ['Stages'],
      datasets: [{
        data: [
          1213
        ],
        backgroundColor: [
          dashboardColors.gossip
        ],
        label: 'Enhance'
      },
        {
          data: [
            4567
          ],
          backgroundColor: [
            dashboardColors.white
          ],
          label: 'RPM'
        },
        {
          data: [

            2312
          ],
          backgroundColor: [
            dashboardColors.silverTree
          ],
          label: 'ML'
        }]
    };
  }
}
