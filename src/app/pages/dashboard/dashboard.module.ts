import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppTranslationModule } from '../../app.translation.module';
import { NgaModule } from '../../theme/nga.module';
import { DataFilterPipe } from './dataTables/data-filter.pipe';
import { DataTableModule } from 'angular2-datatable';
import * as _ from 'lodash';

import { Dashboard } from './dashboard.component';
import { routing } from './dashboard.routing';

import { MyDatePickerModule } from 'mydatepicker';

import { PopularApp } from './popularApp';
import { PieChart } from './pieChart';
import { TrafficChart } from './trafficChart';
import { StackChart } from './stackChart';
import { UsersMap } from './usersMap';
import { LineChart } from './lineChart';
import { Feed } from './feed';
import { Todo } from './todo';
import { Calendar } from './calendar';
import { CalendarService } from './calendar/calendar.service';
import { FeedService } from './feed/feed.service';
import { LineChartService } from './lineChart/lineChart.service';
import { PieChartService } from './pieChart/pieChart.service';
import { TodoService } from './todo/todo.service';
import { TrafficChartService } from './trafficChart/trafficChart.service';
import { StackChartService } from './stackChart/stackChart.service';
import { UsersMapService } from './usersMap/usersMap.service';

import { DataTables } from './dataTables/dataTables.component';
import { DataTablesService } from './dataTables/dataTables.service';

import { ObjectForPipe } from './pipes';

import { HttpModule } from '@angular/http';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AppTranslationModule,
    NgaModule,
    routing,
    DataTableModule,
    MyDatePickerModule,
    HttpModule
  ],
  declarations: [
    PopularApp,
    PieChart,
    TrafficChart,
    StackChart,
    UsersMap,
    LineChart,
    Feed,
    Todo,
    Calendar,
    Dashboard,
    DataTables,
    DataFilterPipe,
    ObjectForPipe
  ],
  providers: [
    CalendarService,
    FeedService,
    LineChartService,
    PieChartService,
    TodoService,
    TrafficChartService,
    UsersMapService,
    StackChartService,
    DataTablesService
  ]
})
export class DashboardModule { }
