import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'ObjectFor'})
export class ObjectForPipe implements PipeTransform {
    transform(value: any, args: any[] = null): any {
    	if(!value)
    		return [];
        return Object.keys(value).map(key => Object.assign({ key }, value[key]));
    }
}