import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpModule } from '@angular/http';

@Injectable()
export class DataTablesService {

  constructor(private http: Http) { }

  // getApiData(): Promise<any[]> {
  //   return this.http.get('10.20.174.2:8181/authenticate')
  //     .toPromise()
  //     .then(response => response.json().data)
  //     .catch(this.handleError);
  // }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  dataTableData = [
    {
      'name': 'npd Batch ID',
      'email': 'tellus.eu.augue@arcu.com',
      'regdate_column': '2016-01-09T14:48:34-08:00',
      'city': 'Paglieta',
      'age': 25,
      'value': 20170503123456,
      'asset': 'global config file',
      'file': 'classification_output_tc_edh_batch_id_20170713081500',
      'md5checksum': 'yes',
      'recordcountmatch': 'yes'
    },
    {
      'name': 'EDH Batch ID',
      'email': 'sed.dictum@Donec.org',
      'regdate_column': '2017-01-23T20:09:52-08:00',
      'city': 'Bear',
      'age': 32,
      'value': 20170503123456,
      'asset': 'global config file',
      'file': 'classification_output_tc_edh_batch_id_20170713081500',
      'md5checksum': 'yes',
      'recordcountmatch': 'yes'
    },
    {
      'name': 'Oliver',
      'email': 'mauris@Craslorem.ca',
      'regdate_column': '2015-11-19T19:11:33-08:00',
      'city': 'Bruderheim',
      'age': 31,
      'value': 20170503123456,
      'asset': 'global config file',
      'file': 'classification_output_tc_edh_batch_id_20170713081500',
      'md5checksum': 'yes',
      'recordcountmatch': 'yes'
    },
    {
      'name': 'Vladimir',
      'email': 'mi.Aliquam@Phasellus.net',
      'regdate_column': '2015-11-02T07:59:34-08:00',
      'city': 'Andenne',
      'age': 50,
      'value': 20170503123456,
      'asset': 'global config file',
      'file': 'classification_output_tc_edh_batch_id_20170713081500',
      'md5checksum': 'yes',
      'recordcountmatch': 'yes'
    },
    {
      'name': 'Maggy',
      'email': 'ligula@acorciUt.edu',
      'regdate_column': '2017-02-25T15:42:17-08:00',
      'city': 'HomprŽ',
      'age': 24,
      'value': 20170503123456,
      'asset': 'global config file',
      'file': 'classification_output_tc_edh_batch_id_20170713081500',
      'md5checksum': 'yes',
      'recordcountmatch': 'yes'
    },
    {
      'name': 'Matthew',
      'email': 'enim.Mauris.quis@vehicula.edu',
      'regdate_column': '2015-05-01T01:53:59-07:00',
      'city': 'Alacant',
      'age': 35,
      'value': 20170503123456,
      'asset': 'global config file',
      'file': 'classification_output_tc_edh_batch_id_20170713081500',
      'md5checksum': 'yes',
      'recordcountmatch': 'yes'
    },
    {
      'name': 'Justina',
      'email': 'Donec.nibh@Vivamusmolestie.ca',
      'regdate_column': '2015-06-24T14:38:07-07:00',
      'city': 'Kobbegem',
      'age': 22,
      'value': 20170503123456,
      'asset': 'global config file',
      'file': 'classification_output_tc_edh_batch_id_20170713081500',
      'md5checksum': 'yes',
      'recordcountmatch': 'yes'
    }
  ];

  dataTableData2 = [
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '1',
      'description': 'Output File',
      'value': 'classification output tc edh batch id 20170713081500'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '2',
      'description': 'global config file',
      'value': 'all global config Data 20170623234907723.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '3',
      'description': 'industry mapping file',
      'value': 'tc industry hierarchy mapping Data 20170713080628277.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '4',
      'description': 'attribute hierarchy mapping file',
      'value': 'tc attribute hierarchy mapping Data 20170623234907723.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '5',
      'description': 'stop words file',
      'value': 'all stop words Data 20170628021621787.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '6',
      'description': 'item description harmonization cleaning rules file',
      'value': 'all harmonization cleaning rules Data 20170706121830330.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '7',
      'description': 'item number harmonization cleaning rules file',
      'value': 'all harmonization cleaning rules Data 20170706121830330.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '8',
      'description': 'nav outlet item dictionary file',
      'value': 'all nav outlet item dictionary Data 20170606192448773.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '9',
      'description': 'nav matching rules file',
      'value': 'all nav matching rules Data 20170706121830330.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '10',
      'description': 'nav outlet item upc lookup file',
      'value': 'all nav outlet item upc lookup Data 20170606192448773.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '11',
      'description': 'checkout to nav map file',
      'value': 'tc checkout to nav map Data 20170602200148197.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '12',
      'description': 'nav matching rules map file',
      'value': 'all nav matching rules map Data 20170706121830330.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '13',
      'description': 'category pattern matching rules file',
      'value': 'tc category pattern matching rules Data 20170623234907723.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '14',
      'description': 'attribute pattern matching rules file',
      'value': 'tc attribute pattern matching rules Data 20170623234907723.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '15',
      'description': 'ml category prep rules file',
      'value': 'tc machine learning evaluation prep rules Data 20170531164741983.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '16',
      'description': 'training category file',
      'value': 'tc training category Data 20170706121830330.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '17',
      'description': 'ml attribute prep rules file',
      'value': 'tc att machine learning evaluation prep rules Data 20170531164741983.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '18',
      'description': 'training attribute file',
      'value': 'tc training attribute Data 20170531164741983.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '1',
      'description': 'Output File',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '2',
      'description': 'global config file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '3',
      'description': 'industry mapping file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '4',
      'description': 'attribute hierarchy mapping file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '5',
      'description': 'stop words file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '6',
      'description': 'item description harmonization cleaning rules file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '7',
      'description': 'item number harmonization cleaning rules file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '8',
      'description': 'nav outlet item dictionary file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '9',
      'description': 'nav matching rules file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '10',
      'description': 'nav outlet item upc lookup file',
      'value': 'no'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '11',
      'description': 'checkout to nav map file',
      'value': 'no'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '12',
      'description': 'nav matching rules map file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '13',
      'description': 'category pattern matching rules file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '14',
      'description': 'attribute pattern matching rules file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '15',
      'description': 'ml category prep rules file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '16',
      'description': 'training category file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '17',
      'description': 'ml attribute prep rules file',
      'value': 'no'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '18',
      'description': 'training attribute file',
      'value': 'no'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '1',
      'description': 'Output File',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '2',
      'description': 'global config file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '3',
      'description': 'industry mapping file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '4',
      'description': 'attribute hierarchy mapping file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '5',
      'description': 'stop words file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '6',
      'description': 'item description harmonization cleaning rules file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '7',
      'description': 'item number harmonization cleaning rules file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '8',
      'description': 'nav outlet item dictionary file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '9',
      'description': 'nav matching rules file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '10',
      'description': 'nav outlet item upc lookup file',
      'value': 'no'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '11',
      'description': 'checkout to nav map file',
      'value': 'no'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '12',
      'description': 'nav matching rules map file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '13',
      'description': 'category pattern matching rules file',
      'value': 'no'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '14',
      'description': 'attribute pattern matching rules file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '15',
      'description': 'ml category prep rules file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '16',
      'description': 'training category file',
      'value': 'no'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '17',
      'description': 'ml attribute prep rules file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '18',
      'description': 'training attribute file',
      'value': 'no'
    },

    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'bs',
      'type': 'Table',
      'col_header': '',
      'sub_type': 'Table1',
      'sort_seq': 1,
      'description': 'NPD Batch ID',
      'value': 20170700000000
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'bs',
      'type': 'Table',
      'col_header': '',
      'sub_type': 'Table1',
      'sort_seq': 2,
      'description': 'EDH Batch ID',
      'value': 1230
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'bs',
      'type': 'Table',
      'col_header': '',
      'sub_type': 'Table1',
      'sort_seq': 3,
      'description': 'Start Time',
      'value': '7/17/2017 10:20'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'bs',
      'type': 'Table',
      'col_header': '',
      'sub_type': 'Table1',
      'sort_seq': 4,
      'description': 'End Time',
      'value': '7/17/2017 15:47'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'bs',
      'type': 'Table',
      'col_header': '',
      'sub_type': 'Table1',
      'sort_seq': 5,
      'description': 'ETA',
      'value': '5:50:00'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'bs',
      'type': 'Table',
      'col_header': '',
      'sub_type': 'Table1',
      'sort_seq': 6,
      'description': 'Execution Time',
      'value': '5:27:30'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'bs',
      'type': 'Table',
      'col_header': '',
      'sub_type': 'Table1',
      'sort_seq': 7,
      'description': 'Records',
      'value': 13544
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': '20170711',
      'layout': 'bs',
      'type': 'Table',
      'col_header': '',
      'sub_type': 'Table1',
      'sort_seq': 8,
      'description': 'Batch Status',
      'value': 'Success'
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': 20170711,
      'layout': 'CS',
      'type': 'Chart',
      'col_header': '',
      'sub_type': 'Chart2',
      'sort_seq': 1,
      'description': 'Resolved and Classified',
      'value': 20000
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': 20170711,
      'layout': 'CS',
      'type': 'Chart',
      'col_header': '',
      'sub_type': 'Chart2',
      'sort_seq': 2,
      'description': 'Not Resolved But Classified',
      'value': 15000
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': 20170711,
      'layout': 'CS',
      'type': 'Chart',
      'col_header': '',
      'sub_type': 'Chart2',
      'sort_seq': 3,
      'description': 'Neither Resolved Nor Classified',
      'value': 2000
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': 20170711,
      'layout': 'CS',
      'type': 'Chart',
      'col_header': '',
      'sub_type': 'Chart3',
      'sort_seq': 1,
      'description': 'Enhance',
      'value': 10000
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': 20170711,
      'layout': 'CS',
      'type': 'Chart',
      'col_header': '',
      'sub_type': 'Chart3',
      'sort_seq': 2,
      'description': 'Pattern Matching',
      'value': 6000
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': 20170711,
      'layout': 'CS',
      'type': 'Chart',
      'col_header': '',
      'sub_type': 'Chart3',
      'sort_seq': 3,
      'description': 'Machine Learning',
      'value': 4000
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': 20170711,
      'layout': 'CS',
      'type': 'Chart',
      'col_header': '',
      'sub_type': 'Chart4',
      'sort_seq': 1,
      'description': 'Enhance',
      'value': 6000
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': 20170711,
      'layout': 'CS',
      'type': 'Chart',
      'col_header': '',
      'sub_type': 'Chart4',
      'sort_seq': 2,
      'description': 'Pattern Matching',
      'value': 5000
    },
    {
      'batch_id': 'tc_Data_20170711121830350',
      'date_column': 20170711,
      'layout': 'CS',
      'type': 'Chart',
      'col_header': '',
      'sub_type': 'Chart4',
      'sort_seq': 3,
      'description': 'Machine Learning',
      'value': 4000
    },
    // new mock batchid
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '1',
      'description': 'Output File',
      'value': 'classification output tc edh batch id 20170713081500'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '2',
      'description': 'global config file',
      'value': 'all global config Data 20170623234907723.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '3',
      'description': 'industry mapping file',
      'value': 'tc industry hierarchy mapping Data 20170713080628277.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '4',
      'description': 'attribute hierarchy mapping file',
      'value': 'tc attribute hierarchy mapping Data 20170623234907723.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '5',
      'description': 'stop words file',
      'value': 'all stop words Data 20170628021621787.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '6',
      'description': 'item description harmonization cleaning rules file',
      'value': 'all harmonization cleaning rules Data 20170706121830330.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '7',
      'description': 'item number harmonization cleaning rules file',
      'value': 'all harmonization cleaning rules Data 20170706121830330.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '8',
      'description': 'nav outlet item dictionary file',
      'value': 'all nav outlet item dictionary Data 20170606192448773.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '9',
      'description': 'nav matching rules file',
      'value': 'all nav matching rules Data 20170706121830330.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '10',
      'description': 'nav outlet item upc lookup file',
      'value': 'all nav outlet item upc lookup Data 20170606192448773.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '11',
      'description': 'checkout to nav map file',
      'value': 'tc checkout to nav map Data 20170602200148197.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '12',
      'description': 'nav matching rules map file',
      'value': 'all nav matching rules map Data 20170706121830330.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '13',
      'description': 'category pattern matching rules file',
      'value': 'tc category pattern matching rules Data 20170623234907723.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '14',
      'description': 'attribute pattern matching rules file',
      'value': 'tc attribute pattern matching rules Data 20170623234907723.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '15',
      'description': 'ml category prep rules file',
      'value': 'tc machine learning evaluation prep rules Data 20170531164741983.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '16',
      'description': 'training category file',
      'value': 'tc training category Data 20170706121830330.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '17',
      'description': 'ml attribute prep rules file',
      'value': 'tc att machine learning evaluation prep rules Data 20170531164741983.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'Files used',
      'sub_type': 'table1',
      'sort_seq': '18',
      'description': 'training attribute file',
      'value': 'tc training attribute Data 20170531164741983.psv'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '1',
      'description': 'Output File',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '2',
      'description': 'global config file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '3',
      'description': 'industry mapping file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '4',
      'description': 'attribute hierarchy mapping file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '5',
      'description': 'stop words file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '6',
      'description': 'item description harmonization cleaning rules file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '7',
      'description': 'item number harmonization cleaning rules file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '8',
      'description': 'nav outlet item dictionary file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '9',
      'description': 'nav matching rules file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '10',
      'description': 'nav outlet item upc lookup file',
      'value': 'no'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '11',
      'description': 'checkout to nav map file',
      'value': 'no'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '12',
      'description': 'nav matching rules map file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '13',
      'description': 'category pattern matching rules file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '14',
      'description': 'attribute pattern matching rules file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '15',
      'description': 'ml category prep rules file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '16',
      'description': 'training category file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '17',
      'description': 'ml attribute prep rules file',
      'value': 'no'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'md5 checksum',
      'sub_type': 'table1',
      'sort_seq': '18',
      'description': 'training attribute file',
      'value': 'no'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '1',
      'description': 'Output File',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '2',
      'description': 'global config file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '3',
      'description': 'industry mapping file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '4',
      'description': 'attribute hierarchy mapping file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '5',
      'description': 'stop words file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '6',
      'description': 'item description harmonization cleaning rules file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '7',
      'description': 'item number harmonization cleaning rules file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '8',
      'description': 'nav outlet item dictionary file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '9',
      'description': 'nav matching rules file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '10',
      'description': 'nav outlet item upc lookup file',
      'value': 'no'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '11',
      'description': 'checkout to nav map file',
      'value': 'no'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '12',
      'description': 'nav matching rules map file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '13',
      'description': 'category pattern matching rules file',
      'value': 'no'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '14',
      'description': 'attribute pattern matching rules file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '15',
      'description': 'ml category prep rules file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '16',
      'description': 'training category file',
      'value': 'no'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '17',
      'description': 'ml attribute prep rules file',
      'value': 'yes'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'dq',
      'type': 'Matrix',
      'col_header': 'record count',
      'sub_type': 'table1',
      'sort_seq': '18',
      'description': 'training attribute file',
      'value': 'no'
    },

    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'bs',
      'type': 'Table',
      'col_header': '',
      'sub_type': 'Table1',
      'sort_seq': 1,
      'description': 'NPD Batch ID',
      'value': 20170700000000
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'bs',
      'type': 'Table',
      'col_header': '',
      'sub_type': 'Table1',
      'sort_seq': 2,
      'description': 'EDH Batch ID',
      'value': 1230
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'bs',
      'type': 'Table',
      'col_header': '',
      'sub_type': 'Table1',
      'sort_seq': 3,
      'description': 'Start Time',
      'value': '7/17/2017 10:20'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'bs',
      'type': 'Table',
      'col_header': '',
      'sub_type': 'Table1',
      'sort_seq': 4,
      'description': 'End Time',
      'value': '7/17/2017 15:47'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'bs',
      'type': 'Table',
      'col_header': '',
      'sub_type': 'Table1',
      'sort_seq': 5,
      'description': 'ETA',
      'value': '5:50:00'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'bs',
      'type': 'Table',
      'col_header': '',
      'sub_type': 'Table1',
      'sort_seq': 6,
      'description': 'Execution Time',
      'value': '5:27:30'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'bs',
      'type': 'Table',
      'col_header': '',
      'sub_type': 'Table1',
      'sort_seq': 7,
      'description': 'Records',
      'value': 13544
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': '20170711',
      'layout': 'bs',
      'type': 'Table',
      'col_header': '',
      'sub_type': 'Table1',
      'sort_seq': 8,
      'description': 'Batch Status',
      'value': 'Success'
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': 20170711,
      'layout': 'CS',
      'type': 'Chart',
      'col_header': '',
      'sub_type': 'Chart2',
      'sort_seq': 1,
      'description': 'Resolved and Classified',
      'value': 40000
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': 20170711,
      'layout': 'CS',
      'type': 'Chart',
      'col_header': '',
      'sub_type': 'Chart2',
      'sort_seq': 2,
      'description': 'Not Resolved But Classified',
      'value': 17000
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': 20170711,
      'layout': 'CS',
      'type': 'Chart',
      'col_header': '',
      'sub_type': 'Chart2',
      'sort_seq': 3,
      'description': 'Neither Resolved Nor Classified',
      'value': 3000
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': 20170711,
      'layout': 'CS',
      'type': 'Chart',
      'col_header': '',
      'sub_type': 'Chart3',
      'sort_seq': 1,
      'description': 'Enhance',
      'value': 8000
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': 20170711,
      'layout': 'CS',
      'type': 'Chart',
      'col_header': '',
      'sub_type': 'Chart3',
      'sort_seq': 2,
      'description': 'Pattern Matching',
      'value': 4300
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': 20170711,
      'layout': 'CS',
      'type': 'Chart',
      'col_header': '',
      'sub_type': 'Chart3',
      'sort_seq': 3,
      'description': 'Machine Learning',
      'value': 3500
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': 20170711,
      'layout': 'CS',
      'type': 'Chart',
      'col_header': '',
      'sub_type': 'Chart4',
      'sort_seq': 1,
      'description': 'Enhance',
      'value': 1200
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': 20170711,
      'layout': 'CS',
      'type': 'Chart',
      'col_header': '',
      'sub_type': 'Chart4',
      'sort_seq': 2,
      'description': 'Pattern Matching',
      'value': 4400
    },
    {
      'batch_id': 'tc_Data_20170711121830351',
      'date_column': 20170711,
      'layout': 'CS',
      'type': 'Chart',
      'col_header': '',
      'sub_type': 'Chart4',
      'sort_seq': 3,
      'description': 'Machine Learning',
      'value': 5500
    }
  ];

  getData(): Promise<any> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(this.dataTableData);
      }, 2000);
    });
  }

  getMainData(): Promise<any> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(this.dataTableData2);
      }, 2000);
    });
  }
}
