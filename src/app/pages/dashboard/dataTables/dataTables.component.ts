import { Component, OnInit } from '@angular/core';
import { DataTablesService } from './dataTables.service';
import * as _ from 'lodash';

@Component({
  selector: 'data-tables',
  templateUrl: './dataTables.html',
  styleUrls: ['./dataTables.scss']
})
export class DataTables {

  data;
  filterQuery = "";
  rowsOnPage = 10;
  sortBy = "email";
  sortOrder = "asc";

  prepareData(data, row) {
    data[row.date] = data[row.date] || {};
    const date = data[row.date];
    date[row.batch_id] = date[row.batch_id] || {};
    const batchId = date[row.batch_id];
    batchId[row.layout] = batchId[row.layout] || {};
    const layout = batchId[row.layout];
    layout[row.type] = layout[row.type] || {};
    const type = layout[row.type];
    type[row.col_header] = type[row.col_header] || {};
    const colHeader = type[row.col_header];
    colHeader[row.sub_type] = colHeader[row.sub_type] || [];
    colHeader[row.sub_type].push(row);
  }


  constructor(private service: DataTablesService) {
    console.log('this is in DataTables component ts file constructor invocation .');
    this.service.getData().then((data) => {
      this.data = data;
      _.forEach(data, function(row) {
        this.prepareData(data, row);
      });

    });
  }

  toInt(num: string) {
    return +num;
  }

  sortByWordLength = (a: any) => {
    return a.city.length;
  }

}