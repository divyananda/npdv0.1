import {Injectable} from '@angular/core';
import {BaThemeConfigProvider, colorHelper} from '../../../theme';

@Injectable()
export class TrafficChartService {

  constructor(private _baConfig: BaThemeConfigProvider) {
  }

  // getData() {
  //   let dashboardColors = this._baConfig.get().colors.dashboard;
  //   return [
  //     {
  //       value: 2000,
  //       color: dashboardColors.white,
  //       highlight: colorHelper.shade(dashboardColors.white, 15),
  //       label: 'Resolved & Classified',
  //       percentage: 87,
  //       order: 1,
  //     }, {
  //       value: 1500,
  //       color: dashboardColors.gossip,
  //       highlight: colorHelper.shade(dashboardColors.gossip, 15),
  //       label: 'Classified not Resolved',
  //       percentage: 22,
  //       order: 2,
  //     }, {
  //       value: 1000,
  //       color: dashboardColors.silverTree,
  //       highlight: colorHelper.shade(dashboardColors.silverTree, 15),
  //       label: 'Not Resolved Nor Classified',
  //       percentage: 70,
  //       order: 3,
  //     }
  //   ];
  // }

  getData() {
    let dashboardColors = this._baConfig.get().colors.dashboard;
    return {
      labels: ['Resolved & Classified', 'Not Resolved But Classified', 'Neither Resolved Nor Classified'],
      datasets: [{
        data: [
          121213,
          456721,
          12134
        ],
        backgroundColor: [
          dashboardColors.gossip,
          dashboardColors.white,
          dashboardColors.silverTree
        ],
        label: 'Dataset 1'
      }]
    };
  }

}
