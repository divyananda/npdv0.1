import { Component, OnInit } from '@angular/core';
import { DataTablesService } from './dataTables/dataTables.service';
import * as _ from 'lodash';
import { IMyDpOptions } from 'mydatepicker';
import { Ng2UiAuthModule, CustomConfig } from 'ng2-ui-auth';
import { HttpModule } from '@angular/http';
import { Headers, Http, Response } from '@angular/http';
import { AuthService } from 'ng2-ui-auth';
import 'rxjs/add/operator/map';
import * as c3 from 'c3';

@Component({
  selector: 'dashboard',
  styleUrls: ['./dashboard.scss'],
  templateUrl: './dashboard.html'
})

export class Dashboard implements OnInit {

  private myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd.mm.yyyy'
  };

  // Initialized to specific date (09.10.2018).
  private model: Object = { date: { year: 2018, month: 10, day: 9 } };

  data;
  apiData;
  filterQuery = '';
  rowsOnPage = 10;
  sortBy = 'email';
  sortOrder = 'asc';
  results: string[];


  batchDate = '20170711';
  batchId = 'tc_Data_20170711121830350';


  constructor(private service: DataTablesService, private http: Http, private auth: AuthService) {

  }

  private headers = new Headers({
    'Content-Type': 'application/json',
    'queryname': 'classificationstats2ndquery',
    'x-auth-token': this.auth.getToken()
  });

  getApiDataFromInetu() {
    // Make the HTTP request:
    return this.http.get('/web-0.0.1/api/rest/getData', { headers: this.headers }).map((res: Response) => res.json());
  }

  getDataFunction(): void {
    let newData = {};
    let batchids = [];
    this.service.getMainData().then((data) => {
      this.data = data;
      _.forEach(data, function(row) {

        // prepare batchid array to fill batchid dropdown
        const idx = _.indexOf(batchids, row.batch_id);
        if (idx === -1) {
          batchids.push(row.batch_id);
        }

        newData[row.date_column] = newData[row.date_column] || {};
        let date = newData[row.date_column];
        date[row.batch_id] = date[row.batch_id] || {};
        let batchId = date[row.batch_id];
        if (row.layout === 'dq') {
          batchId[row.layout] = batchId[row.layout] || {};
          let layout = batchId[row.layout];
          layout[row.type] = layout[row.type] || {};
          let type = layout[row.type];
          type[row.description] = type[row.description] || {};
          let description = type[row.description];
          description[row.col_header] = row.value;
        }
        else if (row.layout == 'CS') {
          batchId[row.layout] = batchId[row.layout] || {};
          let layout = batchId[row.layout];
          layout[row.type] = layout[row.type] || {};
          let type = layout[row.type];
          type[row.sub_type] = type[row.sub_type] || [];
          type[row.sub_type].push(row);
        }
      });
      console.log('data after ::', newData);
      this.data = newData;
    });


    var chart = c3.generate({
      bindto: '#barchart',
      data: {
        x: 'x',
        columns: [
          ['x', 'Stages'],
          ['Enhance', 1230],
          ['Pattern Matching', 2240],
          ['Machine Learning', 3240],
        ],
        type: 'bar',
        groups: [
          ['Enhance', 'Pattern Matching', 'Machine Learning']
        ],
        colors: {
          'Enhance': '#E9E872',
          'Pattern Matching': '#FF6002',
          'Machine Learning': '#8A46AD'
        },
      },
      legend: {
        //        position: 'right'
      },
      axis: {
        rotated: true,
        x: {
          type: 'category',
          tick: {
            multiline: false
          }
        }
      }
    });


  }

  getDqMetrics(): Object {
    // data[batchDate][batchId].dq.Metrics
    if (!this.data || !this.data[this.batchDate] || !this.data[this.batchDate][this.batchId]) {
      return {};
    }

    if (this.data[this.batchDate][this.batchId].dq) {
      return this.data[this.batchDate][this.batchId].dq.Matrix;
    }

  }

  getCsMetrics(): any[] {


    var pieData = {};
    var pieAreas = [];


    if (!this.data || !this.data[this.batchDate] || !this.data[this.batchDate][this.batchId]) {
      return [];
    }

    if (this.data[this.batchDate][this.batchId].CS) {

      _.forEach(this.data[this.batchDate][this.batchId].CS.Chart.Chart2, function(row) {

        pieAreas.push(row.description);

        pieData[row.description] = row.value;

      });


      var chart = c3.generate({
        bindto: '#myc3chart',
        data: {
          // iris data from R
          json: [pieData],
          keys: {
            value: pieAreas
          },
          type: 'pie',
          colors: {
            'Resolved & Classified': '#FFE29A',
            'Unresolved but Clasified': '#FFA1B5',
            'Neither Resolved Nor Classified': '#86C7F3'
          },
          onclick: function(d, i) {
            console.log('onclick', d, i);

            if (d.id == 'Classified') {
              $('#barchart').css('display', 'block');

            }
          },
          onmouseover: function(d, i) { console.log('onmouseover', d, i); },
          onmouseout: function(d, i) { console.log('onmouseout', d, i); }
        }
      });

      return this.data[this.batchDate][this.batchId].CS.Chart;
    }

  }

  ngOnInit(): void {
    // this.getApiDataFromInetu();
    this.getDataFunction();
    // this.getCsMetrics();
  }

  isValidMD5 = (row: any) => {
    if (row['md5 checksum'] === 'yes') {
      return true;
    }
    return false;
  }

  isValidRC = (row: any) => {
    if (row['record count'] === 'yes') {
      return true;
    }
    return false;
  }

  toInt(num: string) {
    return +num;
  }

  sortByWordLength = (a: any) => {
    return a.city.length;
  }

}
