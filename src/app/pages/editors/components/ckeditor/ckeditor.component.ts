import { Component, OnInit } from '@angular/core';

import './ckeditor.loader';
import 'ckeditor';
import { NvD3Module } from 'ng2-nvd3';
import { IMyDpOptions } from 'mydatepicker';


// d3 and nvd3 should be included somewhere
import 'd3';
import 'nvd3';

@Component({
  selector: 'ckeditor-component',
  templateUrl: './ckeditor.html',
  styleUrls: ['./ckeditor.scss']
})

export class Ckeditor implements OnInit {
  private myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd.mm.yyyy'
  };

  // Initialized to specific date (09.10.2018).
  private model: Object = { date: { year: 2018, month: 10, day: 9 } };


  public ckeditorContent: string = '<p>Hello CKEditor</p>';
  public config = {
    uiColor: '#F0F3F4',
    height: '600',
  };

  constructor() {
  }


  options;
  data;
  chartType;

  ngOnInit() {

    this.options = {
      chart: {
        type: 'sunburstChart',
        height: 450,
        //  color: d3.scale.category20c(),
        duration: 250
      }
    };

    this.data = this.sinAndCos();
  }


  sinAndCos() {

    return [{
      'name': 'flare',
      'children': [
        {
          'name': 'Online',
          'children': [

            {
              'name': 'Enhance',
              'children': [
                { 'name': 'Resolved & Classified', 'size': 2983 },
                { 'name': 'Not Resolved but Classified', 'size': 1047 },
                { 'name': 'Not Resolved NOr Classified', 'size': 4375 }
              ]
            },

            {
              'name': 'Rules Pattern Matching',
              'children': [
                { 'name': 'Resolved & Classified', 'size': 5983 },
                { 'name': 'Not Resolved but Classified', 'size': 6047 },
                { 'name': 'Not Resolved NOr Classified', 'size': 3375 }
              ]
            },

            {
              'name': 'Machine Learning',
              'children': [
                { 'name': 'Resolved & Classified', 'size': 783 },
                { 'name': 'Not Resolved but Classified', 'size': 2147 },
                { 'name': 'Not Resolved NOr Classified', 'size': 5475 }
              ]
            }
          ]
        },
        {
          'name': 'Total Channel',
          'children': [
            {
              'name': 'Enhance',
              'children': [
                { 'name': 'Resolved & Classified', 'size': 1983 },
                { 'name': 'Not Resolved but Classified', 'size': 2047 },
                { 'name': 'Not Resolved NOr Classified', 'size': 1375 }
              ]
            },

            {
              'name': 'Rules Pattern Matching',
              'children': [
                { 'name': 'Resolved & Classified', 'size': 1983 },
                { 'name': 'Not Resolved but Classified', 'size': 2047 },
                { 'name': 'Not Resolved NOr Classified', 'size': 1375 }
              ]
            },

            {
              'name': 'Machine Learning',
              'children': [
                { 'name': 'Resolved & Classified', 'size': 1983 },
                { 'name': 'Not Resolved but Classified', 'size': 2047 },
                { 'name': 'Not Resolved NOr Classified', 'size': 1375 }
              ]
            }
          ]
        }
      ]
    }];

  }

}
