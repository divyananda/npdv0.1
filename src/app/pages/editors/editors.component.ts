import {Component} from '@angular/core';
import { IMyDpOptions } from 'mydatepicker';

@Component({
  selector: 'editors',
  template: `<router-outlet></router-outlet>`
})
export class Editors {
  constructor() {
  }
}
