import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CKEditorModule } from 'ng2-ckeditor';
import { NgaModule } from '../../theme/nga.module';

import { routing }       from './editors.routing';
import { Editors } from './editors.component';
import { Ckeditor } from './components/ckeditor/ckeditor.component';

import { LineChart } from './lineChart';
import { LineChartService } from './lineChart/lineChart.service';

import { StackChart } from './stackChart';
import { StackChartService } from './stackChart/stackChart.service';
import { NvD3Module } from 'ng2-nvd3';

// d3 and nvd3 should be included somewhere
import 'd3';
import 'nvd3';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    CKEditorModule,
    routing,
    NvD3Module,
  ],
  declarations: [
    Editors,
    Ckeditor,
    LineChart,
    StackChart
  ],
  providers: [
    LineChartService,
    StackChartService
  ]
})
export class EditorsModule {
}
